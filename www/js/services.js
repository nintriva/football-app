angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('$API', function ($state,$http,$rootScope,$filter,$ionicLoading,$timeout) {
	"use strict";
	 //var host='/api/web/';
	 
	 var host = 'http://api.football-data.org/';
	 return{
			 getCompetitions:function($scope){
				 //var q = $q.defer();
				 var url = host + 'v1/competitions/';
				 //console.log(data);
				 $http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.leagueCodes = [];	
						 $scope.competitions = data.data;
						 console.log($scope.competitions);
						 angular.forEach($scope.competitions, function(value, key){
							$scope.leagueCodes.push(value.league);
						 });
						 console.log($scope.leagueCodes);
						 $scope.currentSeason = $scope.competitions[0].year;
						 console.log($scope.currentSeason);
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });

			 },
			 
			 getCompetitionsFilter:function($scope,season){
				 //var q = $q.defer();
				 var url = host + 'v1/competitions/?season='+season.name;
				 //console.log(data);
				 $http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.competitions = data.data;
						 console.log($scope.competitions);
						 $scope.currentSeason = $scope.competitions[0].year;
						 console.log($scope.currentSeason);
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
				 

			 },
			getTeams:function($scope,competition) {
				console.log(competition);
				$rootScope.competitionName = competition.caption;
				var url = competition._links.teams.href;
				//var url = host + 'v1/competitions/' +competition.id+'/teams';	
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.teams = data.data.teams;
						 console.log($scope.teams);
						 $state.go('tab.competition', {teamsList:$scope.teams});
						 //console.log()
						 
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });		
			},
			
			getLeagueTable:function($scope,competition) {
				console.log(competition);
				$rootScope.competitionName = competition.caption;
				$rootScope.competitionId = competition.id;
				var url = host + 'v1/competitions/' +competition.id+'/leagueTable';	
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.leagueTable = data.data;
						 console.log($scope.leagueTable);
						$state.go('tab.league-table', {tableList:$scope.leagueTable});
						 
						 //console.log()
						 
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
			}, 
			
			getLeagueTableFilter:function($scope,match,competitionId) {
			
				console.log(competitionId);
				console.log(match);
				//$rootScope.competitionName = competition.caption;
				var url = host + 'v1/competitions/' +competitionId+'/leagueTable?matchday='+match;	
				console.log(url);
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
					if(data.data.standing)
						{
						 $scope.leagueTable = data.data.standing;
						 console.log($scope.leagueTable);
						}
					else
						{
						//$scope.leagueTable1 = [];		
						$scope.leagueTable1 = data.data.standings;
						/*
						 angular.forEach($scope.leagues,function(value,key){
							angular.forEach(value,function(data){
								$scope.leagueTable1.push(data);
								
							})})
							console.log($scope.leagueTable1);
							*/
						 }
						 
						//$state.go('tab.league-table', {tableList:$scope.leagueTable});
						 
						 //console.log()
						 
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
			}, 
			getCompetitionFixtures:function($scope,competition) {
				$rootScope.competitionName = competition.caption;
				var url = host + 'v1/competitions/' + competition.id + '/fixtures';	
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 console.log(data.data);	
						 $scope.competitionFixtures = data.data;
						 console.log($scope.competitionFixtures);
						 $state.go('tab.competition-fixtures',{fixtureList:$scope.competitionFixtures})
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });		
			},
			getCompetitionFixturesFilter:function($scope,match,Url) {
			console.log(Url);
				//$rootScope.competitionName = competition.caption;
				var url = Url + '/fixtures?matchday=' + match;	
				console.log(url)
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 console.log(data.data);
						 $scope.fixtureDetails = data.data.fixtures;
						console.log($scope.fixtureDetails);	
						 $scope.competitionFixtures = data.data;
						 console.log($scope.competitionFixtures);
						 //$state.go('tab.competition-fixtures',{fixtureList:$scope.competitionFixtures})
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });		
			},
			
			viewFixturesSet:function($scope,type,value,league) {
			  

			 $scope.linksArray = [];
				console.log(league);
				if((type == 'p' || type=='n')&& league!='')
				{
					var url = host + 'v1/fixtures?timeFrame='+ type + value + '&league=' + league;
					console.log(url);
				}
				else if((type == 'p' || type=='n') && league=='')
				{
					var url = host + 'v1/fixtures?timeFrame='+ type + value;
					
				}
				else if(league!='')
				{
					var url = host + '/v1/fixtures?league='+league;
					console.log(url);

				}
				else
				{

					var url = host + '/v1/fixtures';
				}
				//var url = host + '/v1/fixtures/';
				$scope.getLists = function() {
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 console.log(data.data);
						 $scope.fixtureSet = data.data.fixtures;
						 console.log($scope.fixtureSet);
						  $ionicLoading.hide();
						 $scope.groupedFixtures = $filter('groupBy')($scope.fixtureSet, '_links.competition.href');
						 
						 console.log($scope.groupedFixtures);
						 angular.forEach($scope.groupedFixtures, function(value, key, index){
							$scope.getCompetitionNames(key,value,index);
							
							});		
							//console.log($scope.linksArray);
							//$scope.getCompetitionNames($scope.linksArray);
						// $scope.fixturesSet = data.data;
					
						 //$state.go('tab.competition-fixtures',{fixtureList:$scope.competitionFixtures})
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
					}
					$scope.getLists();
					$ionicLoading.show({
					template: '<ion-spinner icon="spiral" class="spinner-positive"></ion-spinner> <br>Loading...',
					noBackdrop: true,
					animation: 'fade-in'
				  });
					$scope.getCompetitionNames = function(link,value,index) {
						//console.log(array);
						//array.forEach(function(elem,index) {
							//console.log(elem);
							//$scope.getLinks(elem);
							$http({
							method: 'GET',
							url: link,
							headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
						  }).then(function successCallback(data){
							$scope.groupedFixtures[link].name = data.data;
							console.log($scope.groupedFixtures);
							angular.forEach($scope.groupedFixtures,function(elem,index) {
								$scope.fixturesByDate = elem;
								console.log($scope.fixturesByDate);
							});
							//$scope.getTeamsList(elem._links.teams.href, index);
						
						 },
						 function errorCallback(data){
							 console.log("Error");
							 //q.reject(data);
						});
							}
						// $scope.links = link;        
						 
						
						
						/*
						$http({
							method: 'GET',
							url: link,
							headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
						  }).then(function successCallback(data){
							console.log(data.data);
							$scope.fixtureSet[index].captions = data.data;
							console.log($scope.fixtureSet);
						
						 },
						 function errorCallback(data){
							 console.log("Error");
							 //q.reject(data);
						});
						*/
						
						
				},	
			/*		
			timeframeFixtureFilter:function($scope,val,model){
				console.log(val);
				console.log(model);
				var url = host + '/v1/fixtures?timeFrame='+ model + val;
				console.log(url);
				$http({
						 method:'GET',
						 url:url,
						 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
					 })
					  .then(function successCallback(data){
						console.log(data.data);
						$scope.fixtureSet = data.data.fixtures;
						console.log($scope.fixtureSet);
						 
					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
				},
				*/
				
			viewFixture:function($scope,fixture) {
					console.log(fixture);
					var url = fixture._links.self.href;
					console.log(url);
					$http({
						 method:'GET',
						 url:url,
						 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
					 })
					  .then(function successCallback(data){
						 //console.log(data);
						 $scope.detailedFixture = data.data;
						 console.log($scope.detailedFixture);
						 $scope.head2head = data.data.head2head;
						 //console.log($scope.head2head.count);
						 
						 $state.go('tab.fixture',{fixture:$scope.detailedFixture});
					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
			},
			fixtureFilterByHeadCount:function($scope,fixture,head) {
				console.log(fixture);
				console.log(head);
				var url = fixture.fixture._links.self.href + '/?head2head=' + head;
				console.log(url);
				$http({
						 method:'GET',
						 url:url,
						 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
					 })
					  .then(function successCallback(data){
						 //console.log(data);
						 $scope.detailedFixture = data.data;
						 console.log($scope.detailedFixture);
						 $scope.head2head = data.data.head2head;
						 //console.log($scope.head2head.count);
					},
					function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
			},		 
			getPlayers:function($scope,team) {
				//console.log(team.name);
				$rootScope.teamName = team.name;
				//console.log($rootScope.teamName);
				var url = team._links.players.href;	
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.players = data.data.players;
						 //console.log(data);
						 console.log($scope.players);
						 $state.go('tab.team-players', {playersList:$scope.players});
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });		
			},
			
			getTeamFixtures:function($scope,team) {
				//console.log(team.name);
				$rootScope.teamName = team.name;
				//console.log($rootScope.teamName);
				var url = team._links.fixtures.href;	
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						console.log(data);
						 $scope.teamFixtures = data.data;
						 console.log($scope.teamFixtures);
						 //console.log($scope.players);
						 $state.go('tab.team-fixtures', {teamFixturesList:$scope.teamFixtures});
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });		
			},
			getTeamFixturesFilter:function($scope,venue,Url) {
				console.log(venue);
				console.log(Url);
				//console.log(team.name);
				//$rootScope.teamName = team.name;
				//console.log($rootScope.teamName);
				if(venue.name=='All')
				{
					var url = Url;
					console.log(url);
				}
				else
				{
					var url = Url + '?venue=' + venue.name;
					console.log(url);
				}
				$http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.teamFixtures = data.data.fixtures;
						 console.log($scope.teamFixtures);
						 //console.log($scope.players);
						 //$state.go('tab.team-fixtures', {teamFixturesList:$scope.teamFixtures});
						// q.resolve(data);

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });		
			},
			getCompetitionsTeam:function($scope){
				 //var q = $q.defer();
				 var url = host + 'v1/competitions/';
				 //console.log(data);
				 $http({
					 method:'GET',
					 url:url,
					 headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
				 })
				 .then(function successCallback(data){
						 $scope.leagueCodes = [];	
						 $scope.competitions = data.data;
						 console.log($scope.competitions);
						 angular.forEach($scope.competitions, function(value, key){
							$scope.leagueCodes.push(value.league);
						 });
						 console.log($scope.leagueCodes);
						 $scope.currentSeason = $scope.competitions[0].year;
						 console.log($scope.currentSeason);
						// q.resolve(data);
						$scope.competitions.forEach(function (elem,index) {
							$scope.getTeamsList(elem._links.teams.href, index);
						});	

					 },
					 function errorCallback(data){
						 console.log("Error");
						 //q.reject(data);
					 });
					 
				 $scope.getTeamsList = function(link, index) {
					console.log(link);
					$ionicLoading.show({
					template: '<ion-spinner icon="spiral" class="spinner-positive"></ion-spinner> <br>Loading...',
					noBackdrop: true,
					animation: 'fade-in'
				  });
					  $http({
							method: 'GET',
							url: link,
							headers:{'X-Auth-Token':'1b5626e362584bcda2ced6cbf7a09118'}
						  })
						  .then(function successCallback(data){
							console.log(data.data.teams);
							$ionicLoading.hide();
							$scope.competitions[index].teams = data.data.teams;
							console.log($scope.competitions);
						
						 },
						 function errorCallback(data){
							 console.log("Error");
							 //q.reject(data);
						});	
			 }
			 }
			
			
			  
			   
	}});
