$http({
  method: 'GET',
  url: $scope.itemUrl,
}).


success(function(data, status) {
  var result = data;
  var json = JSON.stringify(result);
  var data = JSON.parse(json);
  $scope.req_list = data.result; // response data 
  
  //Loop through requests
  $scope.req_list.foreach(function (elem, index) {
    //chained call is here
    $scope.fetchRequest(elem.price, index);
  });
}).

error(function(data, status) {
  $scope.req_list = [{
    "req_list": "Error fetching list"
  }];
});

$scope.fetchRequest = function(request, index) {
  console.log("Request Number is: " + request);
  $http({
    method: 'GET',
    url: $scope.reqUrl + "/" + request,
  }).

  success(function(data, status) {
    var result = data;
    var json = JSON.stringify(result);
    var data = JSON.parse(json);
    //Attach result of api call to apropriate element of req_list array
    $scope.req_list[index].items = data;

  }).
  error(function(data, status) {
    $scope.req = [{
      "req": "Error fetching list"
    }];
  });

}
}