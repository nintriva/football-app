angular.module('starter.controllers', [])
 .controller('NavController', function($scope, $ionicSideMenuDelegate) {
      $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
      };
    })
	
	
.controller('DashCtrl', function($scope,$state,$stateParams,$API) {
	$API.getCompetitions($scope);
	$scope.matches = [];
	$scope.venues= [{id:1,name:'All'},{id:2,name:'home'},{id:3,name:'away'}];
	$scope.seasons = [{id:1,name:"2016"},{id:2,name:"2015"}];
	console.log($scope.seasons);
	
	$scope.selectedSeason = function(season) {
		console.log(season);
		$API.getCompetitionsFilter($scope,season);
		}
		
	$scope.selectedVenue = function(venue,Url) {
		console.log(venue);
		$API.getTeamFixturesFilter($scope,venue,Url);
		}
			
	$scope.selectedMatchday = function(match,competitionId) {
		console.log(match);
		console.log(competitionId);
		$API.getLeagueTableFilter($scope,match,competitionId);
	}
	$scope.selectedDay = function(match,url) {
		console.log(match);
		console.log(url);
		$API.getCompetitionFixturesFilter($scope,match,url);
		
	}
	
	$scope.selectHeadCount = function(head) {
		console.log(head);
		console.log($scope.detailedFixture);
		$API.fixtureFilterByHeadCount($scope,$scope.detailedFixture,head);
		}
	if($stateParams.fixtureList) {
		console.log($stateParams.fixtureList);
		$scope.count = $stateParams.fixtureList.count;
		//console.log($scope.count);
		$scope.last_item = $scope.count-1;
		$scope.fixtureDetails = $stateParams.fixtureList.fixtures;
		console.log($scope.fixtureDetails);
		//$scope.fixtureDetails1 = $stateParams.fixtureList[0];
		//console.log($scope.fixtureDetails1);
		$scope.competitionUrl = $stateParams.fixtureList.fixtures[0]._links.competition.href;
		$scope.currentday = $stateParams.fixtureList.fixtures[$scope.last_item].matchday;
		console.log($scope.currentday);
		console.log($scope.competitionUrl);
		for(var i=1;i<=$scope.currentday;i++)
		{
			$scope.matches.push(i);
		}
		}
		
	if($stateParams.fixture) {
		$scope.head2headTotal = [];
		console.log($stateParams.fixture);
		$scope.detailedFixture = $stateParams.fixture;
		console.log($scope.detailedFixture);
		$scope.head2headCount = $scope.detailedFixture.head2head.count;
		$scope.h2h = $scope.detailedFixture.head2head;
		console.log($scope.h2h);
		for(var i=1;i<=$scope.head2headCount;i++)
		{
			$scope.head2headTotal.push(i); 
		}
		}
		
	if($stateParams.tableList) {
		//$scope.leagueTable = $stateParams.tableList.standing;
		//console.log($scope.leagueTable);
		console.log($stateParams.tableList);
		if($stateParams.tableList.standings)
			{
			console.log($stateParams.tableList.standings);
			$scope.leagueTable1 = $stateParams.tableList.standings;
			console.log($scope.leagueTable1);
			}
			else
			{
			console.log($stateParams.tableList.standing);
			$scope.leagueTable = $stateParams.tableList.standing;
		}
		//$scope.competitionUrl = $stateParams.tableList._links.competition.href;
		//console.log($scope.competitionUrl);
		$scope.currentday = $stateParams.tableList.matchday;
		console.log($scope.currentday);
		for(var i=1;i<=$scope.currentday;i++)
		{
			$scope.matches.push(i);
		}
		
		}
	
	if($stateParams.playersList){
		$scope.playerDetail = $stateParams.playersList;
		//console.log($scope.playerDetail);
	}
	
	if($stateParams.teamFixturesList) {
		console.log($stateParams.teamFixturesList);
		$scope.competitionUrl = $stateParams.teamFixturesList._links.self.href;
		console.log($scope.competitionUrl);		
		$scope.teamFixtures = $stateParams.teamFixturesList.fixtures;
		console.log($scope.teamFixtures);
		}
	if($stateParams.teamsList){
		$scope.teamDetails = $stateParams.teamsList;
		console.log($scope.teamDetails);
	}
	/*
	if($stateParams.id){
		$scope.competitionId = $stateParams.id;
		//$API.getTeams($scope,$scope.competitionId);
		$API.getLeagueTable($scope,$scope.competitionId);
		$API.getCompetitionFixtures($scope,$scope.competitionId);
		}
	*/	
	$scope.viewCompetitionTeams = function(competition) {
		console.log(competition);
		$API.getTeams($scope,competition);
		//$API.getLeagueTable($scope,competition);
		//$scope.getFixtures = function() {
		//$API.getCompetitionFixtures($scope,competition);
		
	}	
	$scope.viewCompetitionFixures = function(competition) {
		console.log(competition);
		$API.getCompetitionFixtures($scope,competition);
	
	}
	/*
	$scope.viewFixturesSet = function() {
		console.log("fixtures set");
		$API.viewFixturesSet($scope);
	}
	*/
	$scope.viewFixture = function(fixture) {
		console.log(fixture);
		$API.viewFixture($scope,fixture);
	}	
	$scope.viewLeagueTable = function(competition) {
		console.log(competition);
		
		$API.getLeagueTable($scope,competition);
		}
	
	$scope.viewTeamFixures = function(team) {
		console.log(team);
		$API.getTeamFixtures($scope,team);
	}
	
	
	$scope.viewTeamPlayers = function(team) {
		console.log(team);
		$API.getPlayers($scope,team);
	
	}
	$scope.getPlayers = function(team){
		//console.log(team);
		$API.getPlayers($scope,team);
		
	}
})

.controller('FixtureCtrl', function($scope,$API,$ionicLoading) {
	$scope.show = function() {
		$ionicLoading.show({
                template: '<ion-spinner icon="android" class="spinner"></ion-spinner>'
            });
		};
	
 	
	$API.getCompetitions($scope);
	$scope.getRecent = function() {
		$API.viewFixturesSet($scope,'n',1,'');
	}
	$scope.getAll = function() {
		//console.log("All");
		
		$API.viewFixturesSet($scope,'','','');
		}
	
	$scope.previous = function(prev) {
		$scope.prev = prev;
		if($scope.prev!='' && $scope.league!=''&& $scope.league)
			{
				console.log($scope.league);
				$API.viewFixturesSet($scope,'p',$scope.prev,$scope.league);
			}	
		//console.log(prev);
		else{
			
				$API.viewFixturesSet($scope,'p',$scope.prev,'');
			}
	}
	$scope.next = function(next) {
		if(next!='' && $scope.league!=''&& $scope.league)
			{
				console.log($scope.league);
				$API.viewFixturesSet($scope,'n',next,$scope.league);
			}	
		//console.log(prev);
		
		else{
			
				$API.viewFixturesSet($scope,'n',next,'');
			}
		//console.log(next);
		//$API.viewFixturesSet($scope,'n',next,'');
	}
	
	$scope.selectLeague = function(league) {
		$scope.league = league;
		console.log(league);
		$API.viewFixturesSet($scope,'','',league);
	}
	
	$scope.viewFixture = function(fixture) {
		console.log(fixture);
		$API.viewFixture($scope,fixture);
	}
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('TeamCtrl', function($scope,$API) {
	$API.getCompetitionsTeam($scope);
	$scope.viewTeamPlayers = function(team) {
		console.log(team);
		$API.getPlayers($scope,team);
	
	}
	
	$scope.viewTeamFixures = function(team) {
		console.log(team);
		$API.getTeamFixtures($scope,team);
	}
	
	
});
